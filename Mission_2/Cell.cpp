#include "Cell.h"



void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();

}

bool Cell::get_ATP()
{
	Protein * p = new LinkedList[sizeof(LinkedList)];
	p->init(); // Initiates new protein

	std::string transcript= "";
	bool atpBool = false;

	transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene); // Get transcript
	p = this->_ribosome.create_protein(transcript); // Try create protein
	if (p == nullptr)
	{
		std::cerr << "Can't make protein";
		_exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*p); // Build amino acids
	this->_mitochondrion.set_glucose(50); 
	atpBool = this->_mitochondrion.produceATP();
	if (atpBool) // Can produce enough energy
	{
		this->_atp_units = 100;
		return atpBool;
	}
	return atpBool;

}