#include "Ribosome.h"
#include "Protein.h"
#include "AminoAcid.h"
Protein * Ribosome::create_protein(std::string &RNA_transcript) const
{
	Protein* newProtein =  new Protein[sizeof(Protein)]; // Set new pointer
	newProtein->init();
	std::string tempCodin;
	AminoAcid a; // Enum
	int i = 0;
	int l = RNA_transcript.length();

	while(l >=3) // While rna is not empty
	{

		tempCodin = RNA_transcript.substr(0, 3); // Get first 3
		RNA_transcript = RNA_transcript.substr(3) ; // Remove them from RNA
		a = get_amino_acid(tempCodin); 
		if (a == UNKNOWN)
		{
			newProtein->clear(); // Clear Protein
			return nullptr;
		}
		else
		{
			newProtein->add(a); // Add new amino acid
		}
		l = RNA_transcript.length(); // Update RNA length
	}
	return newProtein;
}