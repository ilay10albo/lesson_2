#pragma once
#include <string>
#include <iostream>
using namespace std;

/*
Description:
Input:
Output:
*/
class Gene
{
public:
	/*
	Function initates the gene with start and end index
	input: start , end , on_complementary
	Output: None
	*/
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	/*
	Description: Function returns start index
	Input: None
	Output: start
	*/
	int get_start() const;
	/*
	Description: Function returns the end index
	Input: None
	Output: start index
	*/
	int get_end() const;
	/*
	Description: Function checks if dna is on complementary strand
	Input: None
	Output: True
	*/
	bool is_on_complementary_dna_strand() const;

private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
};

class Nucleus
{
public:
	/*
	Description: Function initiates Nucleus with dna
	Input: Dna sequence
	Output: None
	*/
	void init(const std::string dna_sequence);
	/*
	Description: Function gets rna transscript from gene
	Input: Gene
	Output: string
	*/
	std::string get_RNA_transcript(const Gene& gene) const;
	/*
	Description: Function return a reversed version of DNA strand
	Input: None
	Output: string
	*/
	std::string get_reversed_DNA_strand() const;
	/*
	Description: Function handles the numbers of codons that appear according the the recived codon
	Input: codon string 
	Output: number of appearances
	*/
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
private:
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;
};