#include "Mitochondrion.h"
#include "AminoAcid.h"

void Mitochondrion:: init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	AminoAcid ar[] = { ALANINE, LEUCINE , GLYCINE ,HISTIDINE,LEUCINE,PHENYLALANINE,AMINO_CHAIN_END }; // Amino acid order
	AminoAcidNode * node = protein.get_first(); 
	int acidCounter = 0; // Follow array
	bool take = true; // while amino acid sequance is rapid
	while (node && take) // Node not null and sequance is legit
	{	
		if (ar[acidCounter] != node->get_data())
		{
			take = false; // stop loop
		}
		acidCounter++;
		node = node->get_next();
	}
	if (take) // Enable energy
	{
		this->_has_glocuse_receptor = true;
	}
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	if (this->_has_glocuse_receptor && (this->_glocuse_level>=50))
	{
		return true;
	}return false;
}