#include "Nucleus.h"
using namespace std;


void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start; 
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}
int Gene::get_start() const
{
	return this->_start;
}
int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

void Nucleus::init(const std::string dna_sequence)
{
	this->_DNA_strand = dna_sequence;
	std::string semi = ""; // String builder
	for (unsigned int i = 0; i < dna_sequence.length(); i++) // Create comlementry strand
	{
		switch (dna_sequence[i]) // Depends on specific codon
		{
		case 'A':
			semi += 'T';
			break;
		case 'G':
			semi += 'C';
			break;
		case 'C':
			semi += 'G';
			break;
		case 'T':
			semi += 'A';
			break;
		default:
			std:cerr << "Error , gene is not valid";
			getchar();
			_exit(1);
		}
		
	}
	this->_complementary_DNA_strand = semi; // Update complementary
}


std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string RNA;
	int start = gene.get_start();
	int end = gene.get_end();
	if (gene.is_on_complementary_dna_strand()) // If strand not normal (complementary)
	{
		for (int i = start; i <= end; i++)
		{
			if ((this->_complementary_DNA_strand)[i] == 'T') // If t replace with U
			{
				RNA += 'U';
			}
			else
			{
				RNA += (this->_complementary_DNA_strand)[i]; // Else just add to RNA
			}
		}
	}
	else // If starnd is normal
	{

		for (int i = start; i <= end; i++)
		{
			if ((this->_DNA_strand)[i] == 'T')  // If t replace with U
			{
				RNA += 'U';
			}
			else
			{
				RNA += (this->_DNA_strand)[i]; // Else just add to RNA
			}
		}
	}
	return RNA;
}


std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string foo = this->_DNA_strand; // Original strand
	std::string reverseStrand(foo);
	std::reverse(reverseStrand.begin(), reverseStrand.end()); // Reverse string 
	return reverseStrand;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int count = 0;
	for (size_t offset = this->_DNA_strand.find(codon); offset != std::string::npos;
	offset = this->_DNA_strand.find(codon, offset + codon.length())) // Get number of appearance
	{
		++count;
	}
	return count; 
}
