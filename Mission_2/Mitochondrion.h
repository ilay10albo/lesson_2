#pragma once
#include "Protein.h"

class Mitochondrion
{
public:
	/*
	Description: Function initiates the mitochondrion 
	Input: None
	Output: None
	*/
	void init(); // Initialize 
	/*
	Description: Function checks if a protein can have a glucose receptor and apply to "_has_glocuse_receptor"
	Input: Protain
	Output: none
	*/
	void insert_glucose_receptor(const Protein & protein);
	/*
	Description:sets glucose level
	Input:  glucose units
	Output: none
	*/
	void set_glucose(const unsigned int glocuse_units);
	/*
	Description: Function checks if this mitochonondrion can produce atp 
	Input: none
	Output: none
	*/
	bool produceATP() const;
private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
};