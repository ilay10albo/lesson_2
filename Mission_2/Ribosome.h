#pragma once
#include "Protein.h"


class Ribosome
{
public:
	/*
	Function builds the protiens from 3 codens of an RNA trancsipit
	Input: RNA_transcript which will be divided to three
	Output: New built protien
	*/
	Protein * create_protein(std::string &RNA_transcript) const;
};

