#include "Nucleus.h"
#include <string>
#include <iostream>
using namespace std;

void notmain()
{
	Gene a;
	a.init(2, 4, true);
	//cout << a.is_on_complementary_dna_strand();
	Nucleus e; 
	e.init("GAACATAACA");
	std::string r = e.get_RNA_transcript(a);
	cout <<  "\n RNA-" +r + "\n";
	cout << e.get_reversed_DNA_strand() + "\n";
	cout << e.get_num_of_codon_appearances("AAC");
	getchar();

}