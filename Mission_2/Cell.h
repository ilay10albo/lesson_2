#pragma once
#include "Nucleus.h"
#include "Protein.h"
#include "Ribosome.h"
#include "Mitochondrion.h"
class Cell
{
public:
	/*
	Description: Function gets a dna a gene and sets all of the neccerely organs
	Input: dna , gene
	Output: None
	*/
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	/*
	Description: Function tries ti create a protein that will be able to and get energy at a certain level
	Input: None
	Output:true ,false
	*/
	bool get_ATP();
private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene  _glocus_receptor_gene;
	unsigned int _atp_units;
};